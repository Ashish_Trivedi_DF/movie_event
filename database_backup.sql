-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: cinema_event
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `Username` varchar(50) DEFAULT NULL,
  `Screen_name` varchar(50) DEFAULT NULL,
  `Seat_class` varchar(50) DEFAULT NULL,
  `Seat_no` varchar(300) DEFAULT NULL,
  `Show_time` varchar(20) DEFAULT NULL,
  `Show_date` varchar(20) DEFAULT NULL,
  `Theatre_name` varchar(50) DEFAULT NULL,
  `Booking_id` varchar(50) DEFAULT NULL,
  `Total_cost` int(10) DEFAULT NULL,
  `Service_fee` int(7) DEFAULT NULL,
  `Cost_per_ticket` int(7) DEFAULT NULL,
  `movie_event` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES ('Rahul Sharma','A','GOLD','[3, 4, 5]',NULL,'2019-09-25','VELOCITY','a3383da8-df96-11e9-8818-144f8adc5433',1000,23,341,NULL),('Rahul Sharma','A','GOLD','[3, 4, 5]',NULL,'2019-09-25','VELOCITY','0073dd5e-df9e-11e9-8818-144f8adc5433',1000,23,341,NULL),('Rahul Sharma','A','GOLD','[1, 2, 3]',NULL,'2019-09-25','VELOCITY','603c513a-df9e-11e9-8818-144f8adc5433',1000,23,341,NULL),('Ashish Mishra','A','PLATINIUM','[1, 2, 3]',NULL,'2019-09-25','VELOCITY','103e2a06-dfa2-11e9-8818-144f8adc5433',1000,23,341,NULL),('Ashish Mishra','A','PLATINIUM','[1, 2, 3]',NULL,'2019-09-25','VELOCITY','446c9e66-dfa2-11e9-8818-144f8adc5433',1000,23,341,NULL),('Krishna','PVR','PLATINUM','[3, 5, 7, 8, 9]',NULL,'2019-09-26','Cauvery Theater','b9c1f032-e004-11e9-8e9d-144f8adc5433',900,41,188,NULL),('Swami','PVR','SILVER','[3, 5, 9]',NULL,'2019-09-26','Cauvery Theater','302355cc-e005-11e9-8e9d-144f8adc5433',900,41,314,NULL),('Swami','PVR','SILVER','[3, 5, 9]',NULL,'2019-09-26','Cauvery Theater','35bc1128-e00e-11e9-8e9d-144f8adc5433',900,41,314,'DILWALE'),('Anamika','PVR','SILVER','[3, 5, 9]','3:50','2019-09-26','Cauvery Theater','99719188-e00f-11e9-8e9d-144f8adc5433',900,41,314,'DABABNAGG');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movies` (
  `title` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `rating` varchar(100) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `genre` varchar(40) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES ('Dilwale Dulhania Le Jayenge','U/A','90%','../../assets/images/dilwale.jpg','ROMANTIC','movie'),('Under 25 summit','U/A','70%','../../assets/images/5b6495bf1e49a.image.jpg','Music','event'),('DABANGG','U/A','95%','../../assets/images/dabangg.jpg','ROMANTIC/FIGHT','movie'),('Harry-Potter','U/A','95%','../../assets/images/EW_Harry-Potter_Featured.jpg','ROMANTIC/FIGHT','movie'),('Sholay','U/A','95%','../../assets/images/Sholay-4.jpg','ROMANTIC/FIGHT','movie'),('bahubali','U/A','100%','../../assets/images/bahubali.jpg','ROMANTIC/FIGHT','movie'),('krish','U/A','60%','../../assets/images/krish.jpg','HISTORICAL','movie'),('3 idiots','U/A','90%','../../assets/images/3 idiots.jpg','HISTORICAL','movie'),('ARAKSHAN','U/A','30%','../../assets/images/arakshan.jpg','HISTORICAL','movie'),('Guru Randhava show','U/A','90%','../../assets/images/guru.jpg','CLASSICAL','event'),('DJ NIGHT','U/A','100%','../../assets/images/dj.jpg','POP','event');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-26  9:40:16
