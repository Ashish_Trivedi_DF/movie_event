from flask import request,jsonify,Flask
import json
import mysql.connector
import uuid
from datetime import date
import time

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="datafoundry@123",
    database="cinema_event"
    )
def insert_user_data(user_input,cursor,Booking_id):   
    today = date.today()
    booking_date=str(today)
    t = time.localtime()
    current_time = time.strftime("%H:%M:%S", t) 
    ############
    Username = user_input['Username']
    Screen_name = user_input['Screen_name']
    Seat_class = user_input['Seat_class']
    Seat_no = user_input['Seat_no']
    Show_time =current_time
    Show_date = booking_date
    Theatre_name =user_input['Theatre_name']
    Booking_id = Booking_id
    Total_cost = user_input['Total_cost']
    Service_fee = user_input['Service_fee']
    movie_event =user_input['movie_event']
    Cost_per_ticket = (user_input['Total_cost']+user_input['Service_fee'])/len(Seat_no)

    Show_time =current_time
    sql = "INSERT INTO User (Username, Screen_name,Seat_class,Seat_no,Show_time,Show_date,Theatre_name ,Booking_id,Total_cost,Service_fee,Cost_per_ticket,movie_event) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s,%s)"
    val = (Username,Screen_name,Seat_class,str(Seat_no),Show_time,Show_date,Theatre_name,Booking_id,Total_cost,Service_fee,Cost_per_ticket,movie_event)
    cursor.execute(sql, val)
    mydb.commit()

def delete_record(Booking_id,cursor):
    sql = "DELETE FROM User WHERE Booking_id =%s"
    val = (Booking_id,)
    print(sql,val)

    cursor.execute(sql,val)
    mydb.commit()
    print(cursor.rowcount, "record(s) deleted")

def fetch_data(cursor,type_,cat,Booking_id):
    if cat =="show":
        sql ="SELECT * FROM movies where type =%s"
        val = (type_,)
        cursor.execute(sql, val)
        myresult = cursor.fetchall()
    if cat == "action":
        sql ="SELECT * FROM User where Booking_id =%s"
        val = (Booking_id,)
        cursor.execute(sql, val)
        print(cursor)
        myresult = cursor.fetchall()
        print(myresult)
    return myresult

def update_data(user_input,cursor):
        today = date.today()
        booking_date=str(today)
        Username = user_input['Username']
        Screen_name = user_input['Screen_name']
        Seat_class = user_input['Seat_class']
        Seat_no = user_input['Seat_no']
        Show_time =user_input['Show_time']
        Show_date = booking_date
        Theatre_name =user_input['Theatre_name']
        Booking_id = user_input['Booking_id']
        Total_cost = user_input['Total_cost']
        Service_fee = user_input['Service_fee']
        Cost_per_ticket = (user_input['Total_cost']+user_input['Service_fee'])/len(Seat_no)

        sql = "UPDATE User SET Username = %s, Screen_name = %s ,Seat_class= %s,Seat_no =%s,Show_time=%s, Show_date=%s,Theatre_name=%s,Total_cost=%s,Service_fee=%s,Cost_per_ticket=%s WHERE Booking_id = %s"
        val = (Username,Screen_name,Seat_class,str(Seat_no),Show_time,Show_date,Theatre_name,Total_cost,Service_fee,Cost_per_ticket,Booking_id)
        cursor.execute(sql,val)
        mydb.commit()

        user_updated_info =  fetch_data(cursor,None,'action',Booking_id)
        return user_updated_info
app = Flask(__name__)


@app.route('/booking', methods=['POST'])
def user_registeration():
    user= ["Username","Screen_name" , "Seat_class", "Seat_no", "Theatre_name" ,"Total_cost", "Service_fee","movie_event"]
    print(request.json)
    if  user == list(request.json.keys()):
        print(request)
        user_input=request.get_json()
        print(user_input)
        cursor = mydb.cursor()
        Booking_id = str(uuid.uuid1())
        insert_user_data(user_input,cursor,Booking_id)
        return jsonify({"statusCode": 200, "Booking_id":Booking_id , "Username":user_input['Username'], "success": True}), 200
    return jsonify({"statusCode": 400, "Message":"Please Check and fill requirement Input", "success": True}), 200

@app.route('/movies', methods=['POST'])
def get_movies_event(): 
    user_input=request.get_json()
    print(user_input)
    type_=user_input['type']
    print(type_)
    movie_l=[]
    cursor = mydb.cursor()
    movies = fetch_data(cursor,type_,'show',None)
    
    for mv in movies:
        data= {
                "title" : mv[0],
                "category" : mv[1],
                "rating":mv[2],
                "imgPath" : mv[3],
                "genre" : mv[4]
                }
        print(mv)
        movie_l.append(data)
        data=None
    # data_load=json.dumps({ "list":movie_l})
    # data_load=json.loads(data_load)    
    return jsonify({"statusCode": 200,"Movies":movie_l})



@app.route('/update_booking', methods=['POST'])
def update_user_info():              
        user_input=request.get_json()
        cursor = mydb.cursor()
        update_user_info= update_data(user_input, cursor)
        user_info = []
        for user in update_user_info:
            data= {
                    "Username" : user[0],
                    "Screen_name" : user[1],
                    "Seat_class":user[2],
                    "Seat_no":user[3],
                    "Show_time" :user[4] ,
                    "Show_date" : user[5],
                    "Theatre_name": user[6],
                    "Booking_id":user[7], 
                    "Total_cost": user[8],
                    "Service_fee": user[9],
                    "Cost_per_ticket":user[10] ,
                    "movie_event":user[11]
                    }
            user_info.append(data)
            data=None
        # data_load=json.dumps({ "list":user_info})
        # data_load=json.loads(data_load)    
        return jsonify({"statusCode": 200,"user_updated_info":user_info})



@app.route('/delete', methods=['POST'])
def delete_user_info():
    if  ['Booking_id'] == list(request.json.keys()):    
        user_input=request.get_json()
        Booking_id =user_input['Booking_id']
        cursor = mydb.cursor()
        all_data = fetch_data(cursor,None,'action',Booking_id)
        if len(all_data) ==0:
            return jsonify({"statusCode": 400, "Message":"User Doesn't Exist", "success": True}), 200

        delete_record(Booking_id,cursor)   
        return jsonify({"statusCode": 200 , "Record has been deleted for given id =":Booking_id, "success": True}), 200
    return jsonify({"statusCode": 400, "Message":"Please Check and fill requirement Input", "success": True}), 200

# @app.route('/movie_event', methods=['POST'])
# def show_user_info():
#     if  ['movie_event'] == list(request.json.keys()):    
#         user_input=request.get_json()
#         movie_event =user_input['movie_event']
#         cursor = mydb.cursor()
#         all_data = fetch_data(cursor,None,'action',movie_event)
#         if len(all_data) ==0:
#             return jsonify({"statusCode": 400, "Message":"User Doesn't Exist", "success": True}), 200

#         delete_record(Booking_id,cursor)   
#         return jsonify({"statusCode": 200 , "Record has been deleted for given id =":Booking_id, "success": True}), 200
#     return jsonify({"statusCode": 400, "Message":"Please Check and fill requirement Input", "success": True}), 200


if __name__ == '__main__':
    app.run(debug=True,port=4000)
